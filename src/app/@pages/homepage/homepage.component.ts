import { Component, OnInit } from "@angular/core";

import { DataConextService } from '../../@data/dataContext.service';
import * as data from '../../@data/countryInfo.json';
import { forkJoin, Observable } from 'rxjs';
import { ICityInfo, ICityWeatherInfo } from "src/app/@common/models";

@Component({
    selector: 'app-homepage',
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit {

    // var to load the cityinfo from countryInfoJson 
    // to add more locations update @data/countryInfo.json
    requiredCityInfo: Array<ICityInfo> = (data as any).default.Results;
    // var to load the cities Weather Info from the subscription 
    citiesWeatherInfo: Array<ICityWeatherInfo>;

    constructor(private dataConextService: DataConextService) {

    }

    ngOnInit() {
        this.bootStrpCityWeatherInfo();
    }

    bootStrpCityWeatherInfo(): void {

        // var to contain all the observables of reuqest city weather info
        const $arrayOfWeatherInfoObservables = new Array<Observable<any>>();

        this.requiredCityInfo.forEach(cityInfo => {
            const $obsInfo = this.dataConextService.getWeatherForSpecificLocation(cityInfo);
            $arrayOfWeatherInfoObservables.push($obsInfo);
        });

        // forkJoin to get lazyload all the observables 
        forkJoin($arrayOfWeatherInfoObservables).subscribe(
            (Wdata) => {
                this.citiesWeatherInfo = Wdata;
            }
        )
    }
}