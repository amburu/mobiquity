import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { DataConextService } from "src/app/@data/dataContext.service";
import { map } from "rxjs/operators";
import { ISummaryForcastInfo } from "src/app/@common/models";

@Component({
    selector: 'app-city-weather-forecast',
    templateUrl: './city-weather-forecast.component.html',
    styleUrls: ['./city-weather-forecast.component.css']
})

export class CityWeatherForecastComponent implements OnInit {

    selectedCity: string;
    forcastWeatherInfo: Array<ISummaryForcastInfo>;


    constructor(private dataConextService: DataConextService, private activatedroute: ActivatedRoute) {

    }

    ngOnInit() {

        this.activatedroute.params.subscribe((pramsData: Params) => {
            this.selectedCity = pramsData["Title"];
            this.bootstrapForcastData();
        });

    }

    bootstrapForcastData() {
        const cnt: number = 40;
        const $forcastOBS = this.dataConextService.getForCastForSecificLocation(this.selectedCity, cnt);
        $forcastOBS
            .pipe(
                map((data: ISummaryForcastInfo) => { return data.list })
            ).subscribe((forcastWdata) => {
                // var to define the and filter the output of array of forcast results
                const timeRequiredForForcast = "09:00:00";
                this.forcastWeatherInfo = forcastWdata.filter(element => { return element.dt_txt.includes(timeRequiredForForcast) });
            })
    }


}