import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataConextService } from '../app/@data/dataContext.service';
import { CityWeatherInfoCompnent } from './@common/city-weather-info/city-weather-info.compnent';
import { HomepageComponent } from './@pages/homepage/homepage.component';
import { CityWeatherForecastComponent } from './@pages/city-weather-forecast/city-weather-forecast.component';
import { WeatherForcastComponent } from './@common/weather-forcast/weather-forcast.component';


@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    CityWeatherInfoCompnent,
    CityWeatherForecastComponent,
    WeatherForcastComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [DataConextService],
  bootstrap: [AppComponent]
})
export class AppModule { }
