import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CityWeatherForecastComponent } from './@pages/city-weather-forecast/city-weather-forecast.component';
import { HomepageComponent } from './@pages/homepage/homepage.component';

const routes: Routes = [

  { path: 'Homepage', component: HomepageComponent },
  { path: "Forcast/:Title", component: CityWeatherForecastComponent },
  
  { path: '**', redirectTo: 'Homepage', },
  { path: '', pathMatch: 'full', redirectTo: 'Homepage' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
