

export interface ICityWeatherInfo {
    name?: string;
    dt?: number;
    sys?: any;
    main?: { temp?: string };
}

export interface ICityInfo {
    Country: string;
    City: string;
}

export interface IForcastInfo {
    main: { sea_level: number, temp: number }
    dt: string;
}


export interface ISummaryForcastInfo {
    city: {};
    cnt: number;
    cod: string;
    list: Array<IForcastInfo>;
}