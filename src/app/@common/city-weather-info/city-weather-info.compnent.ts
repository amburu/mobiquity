import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { element } from "protractor";
import { DataConextService } from "src/app/@data/dataContext.service";
import { ICityWeatherInfo } from "../models";



@Component({
    selector: 'app-city-weather-Info',
    templateUrl: './city-weather-info.compnent.html',
    styleUrls: ['./city-weather-info.compnent.css']
})

export class CityWeatherInfoCompnent implements OnInit {


    @Input() CWI: ICityWeatherInfo;

    constructor(private router: Router) {

    }
    ngOnInit() {
    }

    cityOnSelcted(cityName: string): void {
        this.router.navigate([`Forcast/${cityName}`])
    }
}