import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment.prod";

const httpGetOptions = {
    headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json;odata=verbose',
        'Content-Type': 'application/json;odata=verbose',
    }),
};

@Injectable()
export class DataConextService {


    constructor(private http: HttpClient) {

    }


    getWeatherForSpecificLocation(cityInfo: { City: string, Country: string }): any {
        const url = `http://api.openweathermap.org/data/2.5/weather?q=${cityInfo.City},${cityInfo.Country}&appid=${environment.apiKey}&units=Imperial`;
        return this.http.get<any>(url);

    }
    getForCastForSecificLocation(cityName: string, count: number): any {
        const url = `http://api.openweathermap.org/data/2.5/forecast?q=${cityName}&cnt=${count}&appid=${environment.apiKey}&units=Imperial`;
        return this.http.get<any>(url);

    }

}